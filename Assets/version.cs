using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class version : MonoBehaviour
{
    public TextMeshProUGUI textMeshProUGUI;
 
    void Start()
    {
        Invoke(nameof(removetxt), 60f);
        textMeshProUGUI.text ="V "+ Application.version;

       
    }
    void removetxt()
    {
        textMeshProUGUI.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
