using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class ScorePannel : MonoBehaviour
{
    public GameObject pannel;
    public TextMeshProUGUI AllScore;
    public TextMeshProUGUI BurgerScore;
    public TextMeshProUGUI foodScore;
    public GameObject FullBuegerEatingAnimation;
    void Start()
    {
        FoodManager.FoodCompEatedAll += OnEatingAllBuerger;
        FoodManager.EndSession += ShowScore;
    }

    private void OnEatingAllBuerger()
    {
        FullBuegerEatingAnimation.SetActive(false);
        FullBuegerEatingAnimation.SetActive(true);
    }

    private void OnDestroy()
    {
        FoodManager.FoodCompEatedAll -= OnEatingAllBuerger;
        FoodManager.EndSession -= ShowScore;
    }
    public void ShowScore()
    {
        pannel.SetActive(true);
        AllScore.text = ((FoodManager.BurgerEated * 50) + FoodManager.foodEatedNumber * 5).ToString();
        BurgerScore.text = (FoodManager.BurgerEated * 50).ToString();
        foodScore.text = (FoodManager.foodEatedNumber * 5).ToString();

       // Invoke("disableThisGo", 5f);
    }
    void disableThisGo()
    {

        pannel.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
