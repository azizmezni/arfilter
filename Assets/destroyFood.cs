using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyFood : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag.Equals("food"))
            Destroy(collision.gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
