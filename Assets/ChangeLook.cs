using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLook : MonoBehaviour
{
    public List<GameObject> looks= new List<GameObject>();
    public List<GameObject> hats= new List<GameObject>();
    public List<GameObject> Glasses=new List<GameObject>();
    int lookIndex=0;
    int hatIndex=0;
    int glassIndex=0;
    public List<enableDisable> jouerleJeu;
    void Start()
    {
        Application.targetFrameRate = 60;
    }
   
    public void changeLook()
    {
        foreach (var item in looks)
        {
            item.SetActive(false);

        }
        lookIndex++;
        if (lookIndex >= looks.Count)
            lookIndex = 0;
        looks[lookIndex].SetActive(true);
    }
    public void changeHats()
    {
        foreach (var item in hats)
        {
            item.SetActive(false);

        }
        hatIndex++;
        if (hatIndex >= hats.Count)
            hatIndex = 0;
        hats[hatIndex].SetActive(true);
    }
    public void changeGlasses()
    {
        foreach (var item in Glasses)
        {
            item.SetActive(false);

        }
        glassIndex++;
        if (glassIndex >= Glasses.Count)
            glassIndex = 0;
        Glasses[glassIndex].SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
        foreach (var item in jouerleJeu)
        {
            item.go.SetActive(item.isPlaying == FoodManager.instance.IsPlaying);
        }
        
    }
}
[System.Serializable]
public class enableDisable
{
    public GameObject go;
    public bool isPlaying;
}