using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class foodEatedTxt : MonoBehaviour
{
   public TextMeshProUGUI text;
    void Start()
    {
        FoodManager.FoodCompEatedAll += foodEated;
    }
    private void OnDestroy()
    {
        FoodManager.FoodCompEatedAll -= foodEated;
    }
    private void foodEated()
    {
        text.text=FoodManager.BurgerEated.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
