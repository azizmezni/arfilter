using UnityEngine;

public class DestroyWhenOutOfScreen : MonoBehaviour
{
    public RectTransform canvasRect;

    void Update()
    {
        if (!IsInCanvas())
        {
            Destroy(gameObject);
        }
    }

    bool IsInCanvas()
    {
        // Get the position of the image's bottom edge
        float imageBottom = transform.position.y - (transform as RectTransform).rect.height / 2;

        // Check if the image's bottom edge is above the top of the canvas
        return imageBottom >= canvasRect.rect.yMin;
    }
}
