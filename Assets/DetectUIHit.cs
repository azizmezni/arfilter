using UnityEngine;
using static UnityEngine.GridBrushBase;

public class DetectUIHit : MonoBehaviour
{
    private Camera mainCamera;
    FoodTag foodTag;
    public float rotationSpeed = 30f; // Rotation speed in degrees per second.
    RectTransform rectTransform;
    private float currentRotation;
    private int rotationDirection;
    void Start()
    {
        rotationDirection = Random.Range(0, 2) == 0 ? 1 : -1;
        rectTransform = GetComponent<RectTransform>();
        foodTag = GetComponent<foodTagClass>().foodTag;
        mainCamera = Camera.main;
        float randomRotation = Random.Range(0f, 360f);
        rectTransform.eulerAngles = new Vector3(0f, 0f, randomRotation);
        currentRotation = randomRotation;
    }

    void Update()
    {
        // Cast a ray from the camera to the position of the UI element
        Ray ray = mainCamera.ScreenPointToRay(transform.position);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if (hit.collider.CompareTag("Mouth"))
            {
                FoodManager.FoodCompEated(foodTag);
            


            FoodManager.foodEated.Invoke();
                Debug.Log(foodTag);
                Destroy(gameObject);
         
            }
        }
        currentRotation += rotationDirection * rotationSpeed * Time.deltaTime;

        // Ensure the rotation stays within [0, 360] degrees.
        if (currentRotation >= 360f)
        {
            currentRotation -= 360f;
        }
        else if (currentRotation < 0f)
        {
            currentRotation += 360f;
        }

        rectTransform.eulerAngles = new Vector3(0f, 0f, currentRotation);
    }
}

