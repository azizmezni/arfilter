using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class FoodManager : MonoBehaviour
{
    public GameObject new321Go;
    public bool uiVersion;
    public GameObject CanvasSpawner;
    public float timeDuration=30;
    public float timerStart;
    public static FoodManager instance; 
    public Transform Face;
    public float spawnRate = 3f;
    public List<GameObject> Foods = new List<GameObject>();
    public float InitialForce = 50;
    public float foodScale=50;
    public float RangeX = 10;
    public float offsetY = 3;
    public float offsetz = 3;
    public static int foodEatedNumber;
    public static int BurgerEated;
    public static Action foodEated= foodeated;
    public static Action<FoodTag> FoodCompEated= foodComEated;
    public static Action FoodCompEatedAll = FoodEatAll;
    public static Action EndSession = EndGame;
    public bool IsPlaying;
    static HashSet<int> generatedNumbers = new HashSet<int>();
    static Dictionary<int, int> generatedNumbersCount = new Dictionary<int, int>();
    private int minRange = 0; // Minimum value for random number (inclusive)
    private int maxRange = 100; // Maximum value for random number (inclusive)
    private int maxOccurrences = 3; // Maximum allowed occurrences for each number
    private int maxAttempts = 100;
    public GameObject tree21go;
    public FoodComp foodComp;
    private static void FoodEatAll()
    {
        BurgerEated++;
        FindObjectOfType<FoodComp>().ResetBuger();
        generatedNumbersCount.Clear();
        generatedNumbers.Clear();

    }
    private void Resetrandom()
    {
       
    }
    public static void EndGame()
    {
        var go = FindObjectOfType<FoodManager>().gameObject.transform;

        foreach (var item in go.GetComponentsInChildren<Transform>())
        {
            item.localScale = Vector3.one;
        }
    }
    public void startGame()
    {
      //  new321Go.SetActive(true);
        //Invoke(nameof(StartGameold), 0f);
        StartGameold();




    }
    void StartGameold()
    {
        new321Go.SetActive(false);
        CanvasSpawner.SetActive(true);
        maxRange = Foods.Count;
        IsPlaying = true;
        timerStart = Time.realtimeSinceStartup + 0f;
        //InvokeRepeating("spawn", 0.1f, spawnRate);
        foodEatedNumber = 0;
        BurgerEated = 0;
        tree21go.SetActive(true);
        Invoke(nameof(StopCounteron3), 3);
    }
    void StopCounteron3()
    {
        tree21go.SetActive(false);
    }
    private static void foodComEated(FoodTag foodTag )
    {
       FindObjectOfType<FoodComp>().Eat(foodTag );
    }

    private static void foodeated()
    {
        foodEatedNumber++;
    }

    void Start()
    {
        
        instance = this;
        
    }
   public int randomEquilibre()
    {

        var onOff = Random.Range(0, 2);
        if (onOff > 0)
        {

            return Random.Range(0, Foods.Count);
        }
        else
        {

            var missing = foodComp.FoodComponents.Find(x => x.filled == false);
            if (missing != null)
            {
                var tag = missing.tag;
                foreach (var item in Foods)
                {
                    if (item.GetComponent<foodTagClass>().foodTag==missing.tag )
                    {
                        return Foods.IndexOf(item);
                    }                     
                }
                return Random.Range(0, Foods.Count);
            }
            else
            {
                   return Random.Range(0, Foods.Count);
            }


           
        }


    }
    private int GenerateUniqueRandomNumber()
    {
        int randomNum;
        int attempts = 0;

        do
        {
            randomNum = Random.Range(minRange, maxRange ); // +1 to include maxRange
            attempts++;

            if (attempts >= maxAttempts)
            {
                Debug.LogWarning("Exceeded maximum attempts to generate a unique number. Generating any random number.");
                randomNum = Random.Range(minRange, maxRange );
                break;
            }
        }
        while (IsNumberExceedingMaxOccurrences(randomNum) || generatedNumbers.Contains(randomNum));

        if (!generatedNumbersCount.ContainsKey(randomNum))
        {
            generatedNumbersCount[randomNum] = 1;
        }
        else
        {
            generatedNumbersCount[randomNum]++;
        }

        generatedNumbers.Add(randomNum);

        return randomNum;

    }

    private bool IsNumberExceedingMaxOccurrences(int number)
    {
        if (generatedNumbersCount.TryGetValue(number, out int count))
        {
            return count >= maxOccurrences;
        }
        return false;
    }
    public void spawn()

    {

       

        int randomNum = GenerateUniqueRandomNumber();

        

        var v=new Vector3(Random.Range(-RangeX,RangeX),Face.position.y+offsetY,offsetz);
    
        var go= Instantiate( Foods[randomNum],v, Quaternion.identity, transform);
        go.transform.localScale = Vector3.one* foodScale;
        go.GetComponent<Rigidbody>().AddTorque(Vector3.one * Random.Range(10f, 50f));
        go.GetComponent<Rigidbody>().AddForce(Vector3.down * InitialForce*Time.timeSinceLevelLoad);
       

    }


    // Update is called once per frame
    void Update()
    {
        if (timerStart + timeDuration < Time.realtimeSinceStartup&& IsPlaying)
        {
            IsPlaying = false;
            CancelInvoke("spawn");
            CanvasSpawner.SetActive(false);
            FindObjectOfType<FoodComp>().ResetBuger();
            for (int i = 0; i < CanvasSpawner.transform.childCount; i++)
            {
               Destroy( CanvasSpawner.transform.GetChild(i).gameObject);
            }
           
            generatedNumbers.Clear();
            generatedNumbersCount.Clear();
            EndSession?.Invoke();
        }
    }
}
