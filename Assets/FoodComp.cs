using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using System;
using UnityEngine.UI;

public class FoodComp : MonoBehaviour
{
    public float defaultTransparency = 0.5f;
    public List<FoodComponentEntity> FoodComponents= new List<FoodComponentEntity>();

    [Button]
    public void Eat(FoodTag foodTag)
    {

      var foodobj=  FoodComponents.Find(x => x.tag == foodTag&& x.filled==false);
        if(foodobj != null)
        {
            // Enableobject

            foodobj.filled = true;
            foreach(var obj in foodobj.gameobjs)
            {
                var c = obj.color;
                c.a = 1;
                obj.color = c;
            }

        }
        var filledAll = true;
        foreach (var item in FoodComponents)
        {
            if(item.filled == false)
            {
                filledAll = false;
            }
        }
        if (filledAll)
        {
            FoodManager.FoodCompEatedAll?.Invoke();
        }
    }

    [Button]
    public void AllTransparent()
    {
        foreach (var item in GetComponentsInChildren<RawImage>())
        {
            var c = item.color;
            c.a = defaultTransparency;
            item.color = c;
        }
    }
    [Button]
    public void AllNonTransparent()
    {
        foreach (var item in GetComponentsInChildren<RawImage>())
        {
            var c = item.color;
            c.a = 1;
            item.color = c;
        }
    }
    void Start()
    {
        AllTransparent();
    }

    // Update is called once per frame
    void Update()
    {
       
          
       
    }

    public void ResetBuger()
    {
        foreach (var item in FoodComponents)
        {
            item.filled = false;

        }
        AllTransparent();
    }
}
[System.Serializable]

public class FoodComponentEntity
{
    public FoodTag tag;
    public List<RawImage> gameobjs= new List<RawImage>();
   
    public bool filled;
}
public enum FoodTag
{
    fakous,tomato,onion,katchup,lettece,bread,meet
}