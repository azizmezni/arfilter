using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageSpawner : MonoBehaviour
{
    public List<GameObject> imagePrefabs; // List of UI Image prefabs
    public RectTransform canvasRect;
    public float spawnInterval = 1.0f;
    public float imageSpeed = 50f;

    private float spawnTimer;
    private void Start()
    {
        FoodManager.FoodCompEatedAll += foodEated;
        FoodManager.EndSession += ShowScore;
    }
    private void OnEnable()
    {

        generatedNumbersCount.Clear();
        generatedNumbers.Clear();
    }
    private void ShowScore()
    {
        generatedNumbersCount.Clear();
        generatedNumbers.Clear();
    }

    private void foodEated()
    {
        generatedNumbersCount.Clear();
        generatedNumbers.Clear();
    }
    private void OnDestroy()
    {
        FoodManager.FoodCompEatedAll -= foodEated;
        FoodManager.EndSession -= ShowScore;
    }
    void Update()
    {
        spawnTimer += Time.deltaTime;

        if (spawnTimer >= spawnInterval)
        {
            SpawnRandomImage();
            spawnTimer = 0f;
        }
    }
    static HashSet<int> generatedNumbers = new HashSet<int>();
    static Dictionary<int, int> generatedNumbersCount = new Dictionary<int, int>();
    private int minRange = 0; // Minimum value for random number (inclusive)
    private int maxRange = 6; // Maximum value for random number (inclusive)
    private int maxOccurrences = 3; // Maximum allowed occurrences for each number
    private int maxAttempts = 100;
    private int GenerateUniqueRandomNumber()
    {
        int randomNum;
        int attempts = 0;

        do
        {
            randomNum = Random.Range(minRange, maxRange); // +1 to include maxRange
            attempts++;

            if (attempts >= maxAttempts)
            {
                Debug.LogWarning("Exceeded maximum attempts to generate a unique number. Generating any random number.");
                randomNum = Random.Range(minRange, maxRange);
                break;
            }
        }
        while (IsNumberExceedingMaxOccurrences(randomNum) || generatedNumbers.Contains(randomNum));

        if (!generatedNumbersCount.ContainsKey(randomNum))
        {
            generatedNumbersCount[randomNum] = 1;
        }
        else
        {
            generatedNumbersCount[randomNum]++;
        }

        generatedNumbers.Add(randomNum);

        return randomNum;

    }
    private bool IsNumberExceedingMaxOccurrences(int number)
    {
        if (generatedNumbersCount.TryGetValue(number, out int count))
        {
            return count >= maxOccurrences;
        }
        return false;
    }
    void SpawnRandomImage()
    {
        if (imagePrefabs.Count == 0)
        {
            Debug.LogError("Image Prefabs list is empty. Add prefabs to the list.");
            return;
        }

        float randomX = Random.Range(canvasRect.rect.xMin, canvasRect.rect.xMax);
        Vector3 spawnPosition = new Vector3(randomX, canvasRect.rect.height, 0);
        GameObject randomImagePrefab = imagePrefabs[GenerateUniqueRandomNumber()];

        GameObject newImage = Instantiate(randomImagePrefab, spawnPosition, Quaternion.identity);
        newImage.transform.SetParent(canvasRect.transform, false);

        Rigidbody2D rb = newImage.AddComponent<Rigidbody2D>();
        rb.gravityScale = 1.0f;
        rb.velocity = Vector2.down * imageSpeed;

        DestroyWhenOutOfScreen script = newImage.AddComponent<DestroyWhenOutOfScreen>();
        script.canvasRect = canvasRect;
    }
}
