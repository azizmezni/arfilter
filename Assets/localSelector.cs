using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization.Settings;

public class localSelector : MonoBehaviour
{
    public GameObject Pannel;
    public int localid;
    bool loading;
    // Start is called before the first frame update
    void Start()
    {
        localid = PlayerPrefs.GetInt("LocalKey", 0);
        changeLocal(localid);
    }

    public void changeLocal(int localid)
    {
        if(loading) return;

      StartCoroutine(SetLocal(localid));
    }
    IEnumerator SetLocal(int localid)
    {
        loading = true;
        yield return LocalizationSettings.InitializationOperation;
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[localid];
        PlayerPrefs.SetInt("LocalKey", localid);
        loading = false;
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            Pannel.SetActive(!Pannel.activeSelf);
        }
        
    }
}
