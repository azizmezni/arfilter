using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;


public class mouth : MonoBehaviour
{
    public static mouth instance;
    private Camera mainCamera;
    public LayerMask uiLayer;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        mainCamera = Camera.main;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag.Equals("food"))
        {

            if (collision.gameObject.GetComponent<foodTagClass>() != null)
            {
                Debug.LogError(collision.gameObject.GetComponent<foodTagClass>().foodTag);
                FoodManager.FoodCompEated(collision.gameObject.GetComponent<foodTagClass>().foodTag);
            }



            FoodManager.foodEated.Invoke();
            Destroy(collision.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("food"))
        {
            if (other.gameObject.GetComponent<foodTagClass>() != null)
            {
                Debug.LogError(other.gameObject.GetComponent<foodTagClass>().foodTag);
                FoodManager.FoodCompEated(other.gameObject.GetComponent<foodTagClass>().foodTag);
            }



            FoodManager.foodEated.Invoke();
            Destroy(other.gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {
    }
}
