using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFader : MonoBehaviour
{
    public float fadeDuration = 1.0f; // Duration of the fade in/out in seconds
    public float targetAlpha = 0.5f; // The alpha value to which the UI Image will fade

    public CanvasGroup canvasGroup;
    private bool isFadingOut = false;
    private void OnEnable()
    {
        StartCoroutine(FadeInOut());
    }
    private void Start()
    {
     
       // StartCoroutine(FadeInOut());
    }

    private IEnumerator FadeInOut()
    {
        while (true)
        {
            if (isFadingOut)
            {
                // Fade out
                while (canvasGroup.alpha > targetAlpha)
                {
                    canvasGroup.alpha -= Time.deltaTime / fadeDuration;
                    yield return null;
                }

                isFadingOut = false;
            }
            else
            {
                // Fade in
                while (canvasGroup.alpha < 1)
                {
                    canvasGroup.alpha += Time.deltaTime / fadeDuration;
                    yield return null;
                }

                isFadingOut = true;
            }
        }
    }
}
