using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class secondCounter : MonoBehaviour
{
    public TextMeshProUGUI txt;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(FoodManager.instance.IsPlaying)
        txt.text = ((int)(FoodManager.instance.timeDuration- (Time.realtimeSinceStartup - FoodManager.instance.timerStart))).ToString();
    }
}
